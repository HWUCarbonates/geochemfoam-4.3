#decompose
echo -e "decomposePar"
decomposePar > decomposePar.out

# run simpleDBSFoam
echo -e "run simpleDBSFoam"
mpiexec -np 4 simpleDBSFoam -parallel > simpleDBSFoam.out
echo -e "reconstruct"
reconstructPar -latestTime > reconstructPar.out

rm -rf processor*

echo -e "processPoroPerm" 
processPoroPerm > processPoroPerm.out

rm *.out
