#!/bin/bash

set -e

blockMesh
cp 0/alpha1.org 0/alpha1
setFields
decomposePar
mpiexec -np 4 interGCFoam  -parallel
reconstructPar
rm -rf proc*
